<?php

/**
 * @file
 * Admin functions for uc_image_product.
 */


function uc_image_product_config(){
  $form = array();
  
  // Get the available uc product classes
  $product_classes = _uc_image_product_get_product_classes();

  // if there are no product classes defined
  if( count($product_classes) == 0){
    // Show a message
  	$store_link = l(t('Manage classes page'), 'admin/store/products/classes');
  	$message .= t('UC Image Product uses Ubercart product classes. No product classes have been defined yet.') .'<br>';
  	$message .= t('You must define a product class before you can configure UC Image Products') .'<br>';
  	$message .= t('Please define product classes on the ') . $store_link .'.';
    drupal_set_message($message, 'warning');
    return;
  }   
  else{  
    // Load the class options
	  $my_class_options = array();
    foreach( $product_classes as $key => $class){
  	 $my_class_options[$key] = $class->name;
    }
  	
	  $form['defaults'] = array(
	    '#type' => 'fieldset',
	    '#collapsible' => TRUE,
	    '#collapsed' => FALSE,
	    '#title' => t('Default values'),
	    '#description' => t('Default values give you the opportunity to enter values for new UC Image Products.'),
	  );
	  
	  $form['defaults']['uc_image_product_default_price'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Default price'),
	    '#description' => t('Enter a default price for a Image Product.'),
	    '#default_value' => variable_get('uc_image_product_default_price', '0.00'),
	    '#size' => 6,
	    '#maxlength' => 6,
	  );
	
	  $form['defaults']['uc_image_product_default_class'] = array(
	    '#type' => 'select',
	    '#title' => t('Product Class'),
	    '#description' => t('What is the default product class.'),
	    '#default_value' => variable_get('uc_image_product_default_class', FALSE),
	    '#options' => $my_class_options,
	  );
  
    
    return system_settings_form($form);
  }
  
}



