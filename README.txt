Requirements
Installation
Usage
Resources
History



Requirements
------------
UC Image Product uses already developed code, so some knowlegde of Drupal, Ubercart and Image is required!

The following modules need to be enabled in order to work properly.

    ubercart
    image

Make sure those modules are working properly before installing UC Image Product.

UC Image Product needs at least the following to be configured:

    Ubercart file download settings at: admin/store/settings/products/edit/features
    Image import settings at: admin/settings/image/image_import



Installation
------------
Before installation, please make sure you have read the requirements for this module.

Install the module by following the next steps....

    Activate UC Image Product like all contributed Drupal modules. See http://drupal.org/documentation/install/modules-themes/modules-5-6
    Create an ubercart product class at: admin/store/products/classes
    Configure UC Image Product at: admin/content/uc_image_product/settings.


Usage
-----
UC Image Product can convert regular drupal image node-types into Ubercart products. In order to do so, you can follow the next steps:

    To get a list of available images to be converted, go to: admin/content/uc_image_product
    Select the images to be converted
    Select the Catalog(s) to be used
    Press the convert button

Have fun!


Resources
---------
- Advanced help pages are included into the module. Please read them.
- Issues for this module can be reported at drupal.org at: http://drupal.org/project/issues/uc_image_product


History
-------
Maintainer(s):
2009 - present: VinceW (http://drupal.org/user/419825)

Commit history:
http://drupal.org/node/530450/commits

  