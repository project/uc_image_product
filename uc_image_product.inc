<?php


/**
 * @file
 * Helper functions for uc_image_product.
 */



/*
 * Return an array of product class objects
 */
function _uc_image_product_get_product_classes(){
  $retval = array();
  $result = db_query("SELECT * FROM {uc_product_classes} ORDER BY name DESC");
  
  while ($class = db_fetch_object($result)) {
    $retval[$class->pcid] = $class;
  }
  
  return $retval;
}



function _attach_file_download( $args) {

  $nid = $args['last_nid'];
  $file_id = $args['file_id'];
  $fid = 'file';
  $description = $args['description'];

  //drupal_set_message( $description .' created' );

  db_query("INSERT INTO {uc_product_features} (nid,fid,description) VALUES (%d, '%s', '%s')", $nid, $fid, $description);
  $pfid = db_last_insert_id('uc_product_features', 'pfid');
  db_query("INSERT INTO {uc_file_products} (pfid,fid) VALUES (%d, %d)", $pfid, $file_id);
}


function _image_convert_batch_finished() {
  drupal_set_message(t('Done..'));
}


/**
 * Select images available
 * Select all the nodes which are:
 * - Image types AND
 * - Published
 * ToDo: Rewrite this query to exclude the images which already are products
 */
function _select_available_images() {
  $retval = array();

  $query = db_rewrite_sql("SELECT n.nid, n.title FROM {node} AS n WHERE n.status=1 AND type='image' ORDER BY n.sticky DESC, n.title ASC");
  $result = db_query( $query );

  while ($tmp_node = db_fetch_object($result)) {

    $image_node = node_load( $tmp_node->nid, NULL, TRUE );
    $image_info = image_get_info($image_node->images['_original']);

    $retval[$image_node->nid] = new stdClass();
    $retval[$image_node->nid]->title = $image_node->title;
    $retval[$image_node->nid]->nid = $image_node->nid;
    $retval[$image_node->nid]->extension = $image_info['extension'];
    $retval[$image_node->nid]->width = $image_info['width'];
    $retval[$image_node->nid]->height = $image_info['height'];
    $retval[$image_node->nid]->size = $image_info['file_size'];
    $retval[$image_node->nid]->source = $image_node->images['_original'];
    $retval[$image_node->nid]->o_image = _get_image_file_info_by_path($image_node->images['_original']);
  }
  return $retval;
}


function _get_image_file_info_by_id( $image_id ) {
  $query = "SELECT * FROM {files} WHERE fid = %d";
  $result = db_query( $query, $image_id);
  $retval = db_fetch_object( $result );

  return $retval;
}

function _get_image_file_info_by_path( $image_path ) {
  $query = "SELECT * FROM {files} WHERE filepath = '%s'";
  $result = db_query( $query, $image_path);
  $retval = db_fetch_object( $result );

  return $retval;
}


